import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { WeatherForecastPeriod } from 'types/weather'

interface ForecastPeriodProps {
  period: WeatherForecastPeriod
}

const ForecastPeriod = ({ period }: ForecastPeriodProps) => {
  const date = new Date(period.startTime)

  return (
    <View style={styles.container}>
      <Text>
        {timeFormatter.format(date)} {period.temperature}º {period.temperatureUnit} -{' '}
        {period.shortForecast} - Wind {period.windSpeed} {period.windDirection}
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 12,
  },
})

const timeFormatter = new Intl.DateTimeFormat(undefined, { hour: 'numeric' })

export default React.memo(ForecastPeriod)
