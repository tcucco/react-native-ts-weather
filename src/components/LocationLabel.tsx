import React from 'react'
import { Text } from 'react-native'
import { Coords } from 'types/geo'

interface LocationLabelProps {
  coords: Coords
}

const LocationLabel = ({ coords }: LocationLabelProps) => {
  if (coords) {
    return (
      <Text>
        Current Location: {coords.latitude.toFixed(4)}, {coords.longitude.toFixed(4)}
      </Text>
    )
  }
  return <Text>Getting current location...</Text>
}

export default React.memo(LocationLabel)
