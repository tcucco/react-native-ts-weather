import React from 'react'
import { Text } from 'react-native'
import { WeatherForecastPeriod } from 'types/weather'
import ForecastPeriod from './ForecastPeriod'

interface ForecastDayProps {
  date: Date
  periods: WeatherForecastPeriod[]
}

const ForecastDay = ({ date, periods }: ForecastDayProps) => (
  <>
    <Text>{dayFormatter.format(date)}</Text>
    {periods.map((period) => (
      <ForecastPeriod key={period.number} period={period} />
    ))}
  </>
)

const dayFormatter = new Intl.DateTimeFormat(undefined, { weekday: 'long' })

export default React.memo(ForecastDay)
