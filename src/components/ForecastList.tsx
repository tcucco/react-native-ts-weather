import React from 'react'
import { useRecoilValue } from 'recoil'
import { forecastHourlyByDateQuery } from 'state/weather'
import { Coords } from 'types/geo'
import ForecastDay from 'components/ForecastDay'

interface ForecastListProps {
  coords: Coords
}

const ForecastList = ({ coords }: ForecastListProps) => {
  const forecastDays = useRecoilValue(forecastHourlyByDateQuery(coords))

  return (
    <>
      {forecastDays.map(([date, periods]) => (
        <ForecastDay date={date} periods={periods} key={date.valueOf()} />
      ))}
    </>
  )
}

export default React.memo(ForecastList)
