import React from 'react'
import { View, StyleSheet } from 'react-native'

const Screen = ({ children }) => <View style={styles.container}>{children}</View>

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    paddingLeft: 12,
    paddingRight: 12,
  },
})

export default React.memo(Screen)
