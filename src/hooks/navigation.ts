import { ReactNode } from 'react'
import { useIsFocused } from '@react-navigation/native'
import { noop } from 'utils/function'

export const useRenderIfFocused = (
  ifFocused: () => ReactNode,
  ifUnfocused: () => ReactNode = noop,
): ReactNode => {
  const isFocused = useIsFocused()

  return isFocused ? ifFocused() : ifUnfocused()
}
