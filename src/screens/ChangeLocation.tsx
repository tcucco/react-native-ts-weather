import React from 'react'
import MapView from 'react-native-maps'
import { StyleSheet, ActivityIndicator } from 'react-native'
import { useRecoilState } from 'recoil'
import { currentLocationState } from 'state/geo'
import { Coords } from 'types/geo'
import { Screen } from 'components'

const ChangeLocation = () => {
  const [geo, setGeo] = useRecoilState(currentLocationState)

  const onRegionChange = React.useCallback((coords: Coords) => setGeo(coords), [setGeo])

  const initialRegion = React.useMemo(() => {
    if (!geo) {
      return null
    }
    return {
      latitude: geo.latitude,
      longitude: geo.longitude,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }
  }, [geo])

  return (
    <Screen>
      {geo ? (
        <MapView
          style={styles.map}
          onRegionChangeComplete={onRegionChange}
          initialRegion={initialRegion}
        />
      ) : (
        <ActivityIndicator />
      )}
    </Screen>
  )
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
})

export default React.memo(ChangeLocation)
