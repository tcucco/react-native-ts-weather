import React from 'react'
import { Text, ScrollView } from 'react-native'
import { Coords } from 'types/geo'
import { Screen, ForecastList, LocationLabel } from 'components'
import { useRenderIfFocused } from 'hooks/navigation'

interface WeatherProps {
  coords: Coords
}

const Weather = ({ coords }: WeatherProps) => {
  return (
    <Screen>
      <LocationLabel coords={coords} />
      {useRenderIfFocused(() => (
        <React.Suspense fallback={<Text>Getting forecast...</Text>}>
          <ScrollView>
            <ForecastList coords={coords} />
          </ScrollView>
        </React.Suspense>
      ))}
    </Screen>
  )
}

export default React.memo(Weather)
