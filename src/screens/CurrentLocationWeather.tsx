import React from 'react'
import { useRecoilState } from 'recoil'
import { currentLocationState } from 'state/geo'
import Weather from 'screens/Weather'

const CurrentLocationWeather = () => {
  const [geo, setGeo] = useRecoilState(currentLocationState)

  React.useEffect(() => {
    if (!geo) {
      navigator.geolocation.getCurrentPosition((newGeo) => setGeo(newGeo.coords))
    }
  }, [geo, setGeo])

  if (!geo) {
    return null
  }

  return <Weather coords={geo} />
}

export default React.memo(CurrentLocationWeather)
