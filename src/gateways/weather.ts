import { WeatherApiResponse, WeatherPoints } from 'types/weather'

const rootUrl = 'https://api.weather.gov'

async function unwrap<T>(response: Response): Promise<T> {
  const data: WeatherApiResponse<T> = await response.json()

  return data.properties
}

export async function execute<T>(url: string): Promise<T> {
  const response = await fetch(url)

  return await unwrap<T>(response)
}

export async function getPoints(latitude: number, longitude: number): Promise<WeatherPoints> {
  const url = `${rootUrl}/points/${latitude.toFixed(4)},${longitude.toFixed(4)}`
  return await execute(url)
}
