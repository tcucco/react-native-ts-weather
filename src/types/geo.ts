export type Coords = {
  latitude: number
  longitude: number
}

export interface Location extends Coords {
  accuracy: number
  altitude: number
  altitudeAccuracy: number
  heading: number
  speed: number
}

export interface Geo {
  coords: Location
  timestamp: number
}
