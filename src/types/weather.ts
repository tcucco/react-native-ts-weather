import { Pair } from 'types/generic'

export interface WeatherApiResponse<T> {
  properties: T
}

export interface WeatherPoints {
  gridX: number
  gridY: number
  forecast: string
  forecastHourly: string
}

export interface WeatherForecastPeriod {
  number: number
  name: string
  startTime: string
  endTime: string
  isDaytime: boolean
  temperature: number
  temperatureUnit: 'F' | 'C'
  temperatureTrend: string
  windSpeed: string
  windDirection: string
  icon: string
  shortForecast: string
  detailedForecast: string
}

export interface WeatherForecast {
  updated: string
  units: string
  generatedAt: string
  elevation: {
    value: number
    unitCode: string
  }
  periods: WeatherForecastPeriod[]
}

export type DatePeriodsPair = Pair<Date, WeatherForecastPeriod[]>
