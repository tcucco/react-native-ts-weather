export const floor = (date: Date): Date => {
  const dt = new Date(date.valueOf())
  dt.setHours(0, 0, 0, 0)
  return dt
}
