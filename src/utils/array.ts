export function groupBy<I, K>(
  getKey: (i: I) => K,
  compareKeys: (k1: K, k2: K) => boolean,
  items: I[],
): Array<[K, I[]]> {
  return items.reduce((acc: Array<[K, I[]]>, currentValue: I) => {
    const last = acc.slice(-1)[0]

    const key = getKey(currentValue)

    if (!last) {
      const next: [K, I[]] = [key, [currentValue]]
      return [...acc, next]
    }

    const [lastKey, lastValues] = last

    if (compareKeys(key, lastKey)) {
      const next: [K, I[]] = [key, [...lastValues, currentValue]]
      return [...acc.slice(0, -1), next]
    }

    const next: [K, I[]] = [key, [currentValue]]
    return [...acc, next]
  }, [])
}
