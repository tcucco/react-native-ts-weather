import { selectorFamily } from 'recoil'
import * as weather from 'gateways/weather'
import { Coords } from 'types/geo'
import { WeatherForecast, WeatherForecastPeriod, DatePeriodsPair } from 'types/weather'
import { groupBy } from 'utils/array'
import { floor } from 'utils/date'

export const gridPointsQuery = selectorFamily({
  key: 'gridPoints',
  get: (coords: Coords) => async () => {
    return await weather.getPoints(coords.latitude, coords.longitude)
  },
})

export const forecastQuery = selectorFamily<WeatherForecast, Coords>({
  key: 'forecast',
  get: (coords: Coords) => async ({ get }) => {
    const points = get(gridPointsQuery(coords))
    const { forecast: url } = points
    return await weather.execute<WeatherForecast>(url)
  },
})

export const forecastHourlyQuery = selectorFamily<WeatherForecast, Coords>({
  key: 'forecastHourly',
  get: (coords: Coords) => async ({ get }) => {
    const points = get(gridPointsQuery(coords))
    const { forecastHourly: url } = points
    return await weather.execute<WeatherForecast>(url)
  },
})

export const forecastHourlyByDateQuery = selectorFamily<DatePeriodsPair[], Coords>({
  key: 'forecastHourlyByDate',
  get: (coords: Coords) => async ({ get }) => {
    const { periods } = await get(forecastHourlyQuery(coords))
    return groupBy<WeatherForecastPeriod, Date>(
      (period) => floor(new Date(period.startTime)),
      (d1, d2) => d1.valueOf() === d2.valueOf(),
      periods,
    )
  },
})
