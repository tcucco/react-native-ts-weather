import { atom } from 'recoil'
import { Coords } from 'types/geo'

export const currentLocationState = atom<Coords>({
  key: 'currentLocation',
  default: null,
})
