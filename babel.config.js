module.exports = function (api) {
  api.cache(true)
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module-resolver',
        {
          root: ['./src'],
          alias: {
            components: './src/components',
            gateways: './src/gateways',
            hooks: './src/hooks',
            navigation: './src/navigation',
            screens: './src/screens',
            state: './src/state',
            types: './src/types',
            utils: './src/utils',
          },
        },
      ],
    ],
  }
}
