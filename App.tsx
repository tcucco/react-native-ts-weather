import 'react-native-gesture-handler'
import React from 'react'
import { Button, SafeAreaView, StyleSheet } from 'react-native'
import { RecoilRoot } from 'recoil'
import { NavigationContainer } from '@react-navigation/native'
import CurrentLocationWeather from 'screens/CurrentLocationWeather'
import ChangeLocation from 'screens/ChangeLocation'
import { Stack } from 'navigation'

const App = () => {
  return (
    <SafeAreaView style={styles.view}>
      <RecoilRoot>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen {...screens.weather} />
            <Stack.Screen {...screens.changeLocation} />
          </Stack.Navigator>
        </NavigationContainer>
      </RecoilRoot>
    </SafeAreaView>
  )
}

const screens = {
  weather: {
    name: 'Weather',
    component: CurrentLocationWeather,
    options: ({ navigation }) => ({
      title: 'Weather',
      headerRight: () => (
        <Button
          onPress={() => navigation.navigate(screens.changeLocation.name)}
          title="Change Location"
        />
      ),
    }),
  },
  changeLocation: {
    name: 'ChangeLocation',
    component: ChangeLocation,
    options: {
      title: 'Change Location',
    },
  },
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: '#fff',
  },
})

export default React.memo(App)
